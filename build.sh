#!/bin/bash

docker stop pp_parcel_mapper
docker rm pp_parcel_mapper
docker build -t khangnguyen/pp_parcel_mapper .
docker run -it --name pp_parcel_mapper --entrypoint /bin/bash -v $PWD:/srv/pp_parcel_mapper -v $PWD/../logs:/srv/logs khangnguyen/pp_parcel_mapper