FROM java:8
RUN apt-get update
RUN apt-get install -y maven

WORKDIR /srv/pp_parcel_mapper
ADD ./maven-settings.xml /usr/share/maven/conf/settings.xml

ADD start.sh ./
RUN chmod 775 start.sh
ENTRYPOINT ["/srv/pp_parcel_mapper/start.sh"]

CMD []