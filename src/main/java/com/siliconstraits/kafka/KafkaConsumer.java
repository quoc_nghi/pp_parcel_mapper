package com.siliconstraits.kafka;

import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.siliconstraits.dynamodb.DynamoDBService;
import com.siliconstraits.model.Parcel;
import com.siliconstraits.model.RequestData;
import com.siliconstraits.normalize.NormalizeDataThread;
import com.siliconstraits.rest.DataIOService;
import kafka.consumer.Consumer;
import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class KafkaConsumer {

    private String OLD_DATA = "OLD_DATA";
    private String NEW_DATA = "NEW_DATA";
    private String UPDATED_DATA = "UPDATED_DATA";

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private static Boolean IS_ASYNC = Boolean.FALSE;
    private String topic;
    private String refinedTopic;
    private Properties consumerProperties;
    private Properties producerProperties;
    private DynamoDBService dynamoDBService;
    private ExecutorService normalizedExecutorService = Executors.newFixedThreadPool(40);
    private ExecutorService producerExecutorService = Executors.newSingleThreadExecutor();
    private DataIOService dataIOService;

    public KafkaConsumer(String topic, String refinedTopic,
                         Properties consumerProperties,
                         Properties producerProperties,
                         DynamoDBService dynamoDBService,
                         DataIOService dataIOService) {
        this.topic = topic;
        this.refinedTopic = refinedTopic;
        this.dynamoDBService = dynamoDBService;
        this.consumerProperties = consumerProperties;
        this.producerProperties = producerProperties;
        this.dataIOService = dataIOService;

        //properties.put("autocommit.enable", KafkaProperties.AUTO_COMMIT);
    }

    public Boolean call() throws Exception {

        kafka.consumer.ConsumerConfig consumerConfig = new kafka.consumer.ConsumerConfig(consumerProperties);
        ConsumerConnector consumerConnector = Consumer.createJavaConsumerConnector(consumerConfig);

        Map<String, Integer> topicCountMap = new HashMap<String, Integer>();
        topicCountMap.put(topic, 1);

        Map<String, List<KafkaStream<byte[], byte[]>>> consumerMap = consumerConnector.createMessageStreams(topicCountMap);
        List<KafkaStream<byte[], byte[]>> streamList = consumerMap.get(topic);
        KafkaStream<byte[], byte[]> stream = streamList.get(0);
        ConsumerIterator<byte[], byte[]> iterator = stream.iterator();

        String message;

        while (iterator.hasNext()) {
            message = new String(iterator.next().message());

            log.info("Received message :" + message);
            try {
                JSONObject jsonMessage = new JSONObject(message);
                //String state = jsonMessage.getString("state");

                //if (state.equalsIgnoreCase(NEW_DATA) || state.equalsIgnoreCase(UPDATED_DATA)) {
                final String forwardMessage = message;
                Parcel parcel = dynamoDBService.fetchItem(jsonMessage.getString("parcel_id"));

                log.info("Fetched Parcel from Dynamo : " + parcel);

                final NormalizeDataThread normalizeDataThread = new NormalizeDataThread(parcel);

                final CompletableFuture<Boolean> future = CompletableFuture.supplyAsync(() -> {
                    try {
                        return normalizeDataThread.call();
                    } catch (Exception e) {
                        log.error(e.getMessage());
                    }
                    return true;
                }, normalizedExecutorService);

                future.whenComplete((t, e) -> {
                    log.info("Then submit to DataIO Reader [" + Thread.currentThread().getName() + "]");
                    RequestData data = new RequestData();

                    try {
                        data.setLast_data_source(jsonMessage.getString("last_data_source"));
                        data.setParcel(parcel);
                        data.setParcel_id(jsonMessage.getString("parcel_id"));
                        data.setState(jsonMessage.getString("state"));

                        boolean submitted = dataIOService.submitToDataReader(data);

                        if(submitted){
                            log.info("Then notify kafka [" + Thread.currentThread().getName() + "]");
                            KafkaProducerThread producerThread = new KafkaProducerThread(refinedTopic, IS_ASYNC, producerProperties, forwardMessage);
                            producerExecutorService.submit(producerThread);
                        }
                    } catch (JsonProcessingException e1) {
                        log.error("Could not response value from Data I/O to json");
                    } catch (JSONException e2) {
                        log.error("Not a well-formed JSON data from Kafka");
                    } catch (Exception e3) {
                        log.error(e.getMessage());
                    }
                });
//                future.thenApply(t -> {
//
//                });
                //}
            } catch (JSONException e) {
                log.error("Not a well-formed JSON data from Kafka");
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        }
        return true;
    }
}