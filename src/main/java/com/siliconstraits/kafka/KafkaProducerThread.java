package com.siliconstraits.kafka;


import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.concurrent.ExecutionException;


public class KafkaProducerThread implements Runnable {
    private final KafkaProducer<String, String> producer;
    private final String topic;
    private final Boolean isAsync;
    private final String message;
    private final Properties properties;
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    public KafkaProducerThread(String topic, Boolean isAsync,Properties properties, String message) {
        this.properties = properties;
//        props.put("metadata.broker.list", "192.168.99.100:9092");
//        props.put("advertised.host.name","192.168.99.100");
//        props.put("advertised.port",9092);
        producer = new KafkaProducer<>(properties);
        this.topic = topic;
        this.isAsync = isAsync;
        this.message = message;
    }

    @Override
    public void run() {

        long startTime = System.currentTimeMillis();
        if (isAsync) { // Send asynchronously
            producer.send(new ProducerRecord<>(topic,
                    String.valueOf(message),
                    message), new OnSentCallBack(startTime, message, message));
            log.info("Sent message to Kafka: (" + message + ")");
        } else { // Send synchronously
            try {
                producer.send(new ProducerRecord<>(topic,
                        String.valueOf(message),
                        message)).get();
                log.info("Sent message to Kafka: (" + message + ")");

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }

    }

}
