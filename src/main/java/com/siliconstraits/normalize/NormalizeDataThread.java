package com.siliconstraits.normalize;

import com.siliconstraits.model.Parcel;
import com.siliconstraits.rest.DataIOService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;

public class NormalizeDataThread implements Callable {

    private Parcel parcel;

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private DataIOService dataIOService;

    public NormalizeDataThread(Parcel parcel){
        this.parcel = parcel;

    }

    @Override
    public Boolean call() throws Exception {
        log.info("Begin normalizing data in thread [" + Thread.currentThread().getName() + "]");

        log.info("Finish normalizing data in thread [" + Thread.currentThread().getName() + "]");
        return true;
    }
}
