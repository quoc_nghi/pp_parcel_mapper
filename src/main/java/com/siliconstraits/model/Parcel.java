package com.siliconstraits.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import java.util.List;

@DynamoDBTable(tableName = "parcel_dev")
public class Parcel {
    private String parcel_id;
    private String carrier_name;
    private List<Event> events;
    private String last_data_source;
    private Long updated_date;

    @Override
    public String toString() {
        StringBuilder parcel = new StringBuilder();
        parcel.append("Parcel");
        if(parcel_id != null){
            parcel.append("[parcel_id=" + parcel_id);
        }
        if(carrier_name != null){
            parcel.append(", carrier_name=" + carrier_name);
        }
        if(last_data_source != null){
            parcel.append(", last_data_source=" + last_data_source);
        }
        if(updated_date!= null){
            parcel.append(", updated_date=" + updated_date + "]");
        }
        if(events!=null){
            parcel.append(", events=" + events.toString()+"]");
        }
//        return "Parcel [parcel_id=" + parcelId + ", carrier_name=" + carrierName
//                + ", last_data_source=" + lastDataSource + ", updated_date=" + updatedDate + "]" + ", events=" + events.toString()+"]";
        return parcel.toString();
    }

    public void setParcel_id(String parcel_id) {
        this.parcel_id = parcel_id;
    }

    @DynamoDBHashKey(attributeName = "parcel_id")
    public String getParcel_id(){
        return this.parcel_id;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    @DynamoDBAttribute(attributeName = "events")
    public List<Event> getEvents(){
        return this.events;
    }

    public void setLast_data_source(String last_data_source) {
        this.last_data_source = last_data_source;
    }

    @DynamoDBAttribute(attributeName = "last_data_source")
    public String getLast_data_source(){
        return this.last_data_source;
    }

    public void setUpdated_date(Long updated_date) {
        this.updated_date = updated_date;
    }

    @DynamoDBAttribute(attributeName = "updated_date")
    public Long getUpdated_date(){
        return this.updated_date;
    }

    public void setCarrier_name(String carrier_name) {
        this.carrier_name = carrier_name;
    }

    @DynamoDBAttribute(attributeName = "carrier_name")
    public String getCarrier_name(){
        return this.carrier_name;
    }
}