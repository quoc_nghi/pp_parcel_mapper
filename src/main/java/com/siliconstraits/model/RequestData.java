package com.siliconstraits.model;

public class RequestData {

    private String last_data_source;
    private String state;
    private String parcel_id;
    private Parcel parcel;

    public Parcel getParcel() {
        return parcel;
    }

    public void setParcel(Parcel parcel) {
        this.parcel = parcel;
    }

    public String getParcel_id() {
        return parcel_id;
    }

    public void setParcel_id(String parcel_id) {
        this.parcel_id = parcel_id;
    }

    public String getLast_data_source() {
        return last_data_source;
    }

    public void setLast_data_source(String last_data_source) {
        this.last_data_source = last_data_source;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
