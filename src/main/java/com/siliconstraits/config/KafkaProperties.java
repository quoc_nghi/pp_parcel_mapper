package com.siliconstraits.config;

public class KafkaProperties {
    public static final String ZOOKEEPER_CONNECT = "192.168.99.100:2181";
    public static final String GROUP_ID = "default";
    public static final String PARTITION_ASSIGNMENT_STRATEGY = "roundrobin";
    public static final String BOOTSTRAP_SERVERS = "192.168.99.100::9092";
    public static final String VALUE_DESERIALIZER = "org.apache.kafka.common.serialization.StringDeserializer";
    public static final String KEY_DESERIALIZER = "org.apache.kafka.common.serialization.StringDeserializer";
    public static final String NEW_PARCEL_DATA_TOPIC = "new_parcel_data";
    public static final String NEW_REFINED_DATA_TOPIC = "new_refined_data";
}
