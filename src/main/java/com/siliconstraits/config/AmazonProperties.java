package com.siliconstraits.config;


public class AmazonProperties {
    public static final String ACCESS_KEY = "amazon.accesskey";
    public static final String SECRET_KEY = "amazon.secretkey";
    public static final String END_POINT = "amazon.endpoint";
}
