package com.siliconstraits.dynamodb;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.siliconstraits.model.Parcel;

public class DynamoDBService {

    private DynamoDBMapper dynamoDBMapper;

    public DynamoDBService(DynamoDBMapper dynamoDBMapper) {
        this.dynamoDBMapper = dynamoDBMapper;
    }

    public Parcel fetchItem(String id) {
        return dynamoDBMapper.load(Parcel.class, id);
    }
}
