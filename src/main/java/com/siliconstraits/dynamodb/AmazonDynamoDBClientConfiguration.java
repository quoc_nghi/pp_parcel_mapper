package com.siliconstraits.dynamodb;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.siliconstraits.config.AmazonProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class AmazonDynamoDBClientConfiguration {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private Environment environment;

    public AmazonDynamoDBClient configDynamo() {
        String accessKey = environment.getProperty(AmazonProperties.ACCESS_KEY);
        String secretKey = environment.getProperty(AmazonProperties.SECRET_KEY);
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKey, secretKey);
        AmazonDynamoDBClient dynamoDBClient = new AmazonDynamoDBClient(awsCreds);

        String[] profiles = environment.getActiveProfiles();
        log.debug("Active profiles: " + profiles[0]);

        if (profiles[0].equalsIgnoreCase("local")) {
            String endpoint = environment.getProperty("amazon.endpoint");
            log.info("Set endpoints : " + endpoint);
            dynamoDBClient.setEndpoint(endpoint);
        }
        else{
            dynamoDBClient.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));
        }

        return dynamoDBClient;
    }
}
