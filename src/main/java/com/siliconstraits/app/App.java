package com.siliconstraits.app;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.siliconstraits.config.KafkaProperties;
import com.siliconstraits.dynamodb.AmazonDynamoDBClientConfiguration;
import com.siliconstraits.dynamodb.DynamoDBService;
import com.siliconstraits.kafka.KafkaConsumer;
import com.siliconstraits.rest.DataIOService;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import java.util.Properties;

@Configuration
@ComponentScan(basePackages = "com.siliconstraits")
@EnableAutoConfiguration
public class App implements CommandLineRunner {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private AmazonDynamoDBClientConfiguration configuration;

    @Autowired
    private Environment environment;

    @Autowired
    private DataIOService dataIOService;

    public static void main(String[] args) {
        SpringApplication.run(App.class);
    }

    @Override
    public void run(String... strings) throws Exception {

        AmazonDynamoDBClient dynamoDBClient = configuration.configDynamo();
        DynamoDBMapper dynamoDBMapper = new DynamoDBMapper(dynamoDBClient);
        DynamoDBService dynamoDBService = new DynamoDBService(dynamoDBMapper);
        Properties consumerProperties = configKafkaPropertyForConsumer();
        Properties producerProperties = configKafkaPropertyForProducer();

        KafkaConsumer consumerThread = new KafkaConsumer(environment.getProperty("new_parcel_data_topic"), environment.getProperty("new_refined_data_topic"),consumerProperties,producerProperties, dynamoDBService, dataIOService);
        consumerThread.call();
    }

    public Properties configKafkaPropertyForConsumer() {
        Properties props = new Properties();
        props.put("zookeeper.connect", environment.getProperty("zookeeper.connect"));
        props.put("group.id", environment.getProperty("group.id"));
        props.put("key.serializer", environment.getProperty("key.serializer"));
        props.put("value.serializer", environment.getProperty("value.serializer"));
        props.put("partition.assignment.strategy", environment.getProperty("partition.assignment.strategy"));
        return props;
    }

    public Properties configKafkaPropertyForProducer() {
        Properties props = new Properties();
        log.warn("Producer properties get successful");
        log.warn(environment.getProperty("bootstrap.servers") + " | " + environment.getProperty("client.id"));
        props.put("bootstrap.servers", environment.getProperty("bootstrap.servers"));
        props.put("client.id", environment.getProperty("client.id"));
        props.put("key.serializer",StringSerializer.class.getName());
        props.put("value.serializer", StringSerializer.class.getName());
        props.put("retries", 0);
        return props;
    }
}