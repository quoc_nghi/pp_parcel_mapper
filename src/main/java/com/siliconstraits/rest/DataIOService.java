package com.siliconstraits.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.siliconstraits.model.Parcel;
import com.siliconstraits.model.RequestData;
import com.siliconstraits.model.ResponseData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class DataIOService {

    private static final String URL = "/api/v1/data-mapping/add-parcel/";
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final RestTemplate restTemplate = new RestTemplate();
    private final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private Environment environment;

    public DataIOService() {

    }

    public boolean submitToDataReader(RequestData data) throws JsonProcessingException {
        ResponseData status = restTemplate.postForObject(environment.getProperty("data.io.context") + URL, data, ResponseData.class);
        log.info("Submitted to dataIO this parcel :" + mapper.writeValueAsString(data));
        log.info(status.getMessage());
        return true;
    }
}
