# How to run
First you need to install java 8. (Skip if you already have java 8 installed on your machine)

* ``brew tap caskroom/cask``
* ``brew install brew-cask``
* ``brew cask install java``

Install maven dependency manager http://mvnvm.org/ via HomeBrew. Maven is a dependency management and build tool for Java project.(Skip if you already have maven installed on your machine)

* ``brew install mvnvm* ``

Make sure PostgreSQL are running in your local machine:

* `docker run -p 5432:5432 --name postgres -d postgres`

### `Run manually`
Clone project and use maven to build:

* ``mvn clean install``

In the case if you want to ignore all the test to accelerate build velocity, just use:

* ``mvn clean install -DskipTests``

Use spring-boot to run project:

* ``mvn spring-boot:run -Dspring.profiles.active=production``  (Run on production mode, which will point to the cloud DynamoDB)
* ``mvn spring-boot:run -Dspring.profiles.active=local``  (Run on local development mode, which will point to the local DynamoDB)

Navigate to localhost:8080 to view the result.

In the case you want to change port, just add the parameter like this : 
* ``mvn spring-boot:run -Dspring.profiles.active=production --server.port = 9000``

### `Run with docker` (Coming soon)